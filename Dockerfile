# # Filename: Dockerfile
# FROM ubuntu:latest
# #WORKDIR /usr/src/app
# #COPY package*.json ./
# #RUN npm install
# CMD ["echo", "Ola por dentro do Container"]

FROM node:18
# Create app directory
WORKDIR /usr/src/app
COPY App/ .
RUN npm install
EXPOSE 3000
#RUN npm start
ENTRYPOINT [ "npm","start" ]
# If you are building your code for production
# RUN npm ci --omit=dev