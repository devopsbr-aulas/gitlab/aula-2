const express = require('express')
const app = express()
const port = 8888
const host = '0.0.0.0'

app.get('/', (req, res) => {
  res.send('Oba estou dentro do Express')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})